import { clientConfig } from 'configs/client.config';
import Client from './client';

export { METHOD } from './client';
export default new Client(clientConfig);
