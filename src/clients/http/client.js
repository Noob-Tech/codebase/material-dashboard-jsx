// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { validateClientConfig } from 'validators/config.validator';
import { validateObject } from 'validators/base';

const ERR_CODE = 'API_ERR';

export const METHOD = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete',
};

export default class Client {
  constructor(clientConfig = { baseURL: '', basePath: '' }) {
    // Validate config
    validateObject(validateClientConfig, clientConfig);

    this.client = axios.create({
      baseURL: clientConfig.baseUrl + clientConfig.basePath,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    throwError('test');
  }

  async handlePOST(path, body, config) {
    return await this.client.post(path, body, config);
  }

  async handleGET(path, params, config) {
    return await this.client.get(path, { params, ...config });
  }

  async handlePUT(path, body, config) {
    return await this.client.put(path, body, config);
  }

  async handleDELETE(path, config) {
    return await this.client.delete(path, config);
  }

  /**
   * @param {'GET'|'POST'|'PUT'|'DELETE'} method - HTTP request methods
   * @param {string} path - API request path
   * @param {object} data - Object data
   * @param {AxiosRequestConfig} config - Axios request config
   * @returns {Promise<AxiosResponse<any>|*>}
   */
  async handleRequest({ method, path, data = {}, config = {} }) {
    try {
      switch (method.toUpperCase()) {
        case METHOD.GET:
          return await this.handleGET(path, data, config);
        case METHOD.POST:
          return await this.handlePOST(path, data, config);
        case METHOD.PUT:
          return await this.handlePUT(path, data, config);
        case METHOD.DELETE:
          return await this.handleDELETE(path, config);
        default:
          throwError('Method not implemented', { code: ERR_CODE });
      }
    } catch (err) {
      const { response } = err;
      if (response) {
        if (response.status === 401) {
          this.unsetAuthorization();
        }
        err.message = response.data.message;
      }

      throw err;
    }
  }

  /**
   * @param {'blank'|'basic'|'bearer'} type - Authorization types
   * @param {string} authorization - Authroziation
   * @returns {Promise<void>}
   */
  async setAuthorization({ type = 'blank', authorization = '' }) {
    // Unset first
    this.unsetAuthorization();

    let Authorization;
    switch (type.toLowerCase()) {
      case 'basic':
        Authorization = `Basic ${authorization}`;
        break;
      case 'bearer':
        Authorization = `Bearer ${authorization}`;
        break;
      default:
        throwError('Unknown auth type', { code: ERR_CODE });
    }

    this.client.defaults.headers.Authorization = Authorization;
  }

  unsetAuthorization() {
    if (this.checkAuthorization()) {
      delete this.client.defaults.headers.Authorization;
    }
  }

  checkAuthorization() {
    return (
      this.client.defaults.headers.Authorization !== '' && this.client.defaults.headers.Authorization !== undefined
    );
  }
}
