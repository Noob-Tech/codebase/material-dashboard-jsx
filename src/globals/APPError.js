class APPError extends Error {
  code;

  source;

  metadata;

  constructor(code, { message, source, metadata } = { message: '', source: null, metadata: null }) {
    super(message || 'APPError');
    this.name = 'APPError';
    this.code = code;
    this.source = source;
    this.metadata = metadata;

    Object.setPrototypeOf(this, APPError.prototype);
  }
}

global.throwError = (message, { code, metadata } = { code: 'APP_ERR', metadata: null }) => {
  throw new APPError(code, { message, metadata });
};
