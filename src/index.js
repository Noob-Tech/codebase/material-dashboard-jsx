import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import store, { persistor } from './redux/store';

import './globals';

import App from './App';
import './assets/scss/app.scss';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);

// Hot reload
module.hot.accept();

serviceWorker.unregister();
