import React from 'react';
import Router from './router';

const App = (props) => <Router {...props} />;

export default App;
