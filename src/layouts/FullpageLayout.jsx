import React from 'react';

function FullPageLayout({ children }) {
  return <div className="Main">{children}</div>;
}

export default FullPageLayout;
