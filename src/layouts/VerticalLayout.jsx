import React, { PureComponent } from 'react';
import classNames from 'classnames';
import { CssBaseline } from '@material-ui/core';
import Appbar from './components/Appbar';
import AppDrawer from './components/AppDrawer';
import Footer from './components/Footer/Footer';

class VerticalLayout extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      drawerOpen: false,
    };
  }

  toggleDrawer = () => this.setState(({ drawerOpen }) => ({ drawerOpen: !drawerOpen }));

  render() {
    const { drawerOpen } = this.state;
    const { children } = this.props;
    return (
      <>
        <CssBaseline />
        <Appbar toggleDrawer={this.toggleDrawer} drawerOpen={drawerOpen} />
        <AppDrawer toggleDrawer={this.toggleDrawer} drawerOpen={drawerOpen} />
        <div className={classNames('Main', { DrawerOpen: drawerOpen })}>{children}</div>
        <Footer className={classNames({ DrawerOpen: drawerOpen })} />
      </>
    );
  }
}

export default VerticalLayout;
