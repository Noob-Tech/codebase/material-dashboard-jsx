import React, { Component } from 'react';

import { Drawer, IconButton, Divider } from '@material-ui/core';
import { ChevronLeft } from '@material-ui/icons';

import DrawerHeader from './Header';
import DrawerMenu from './Menu';
// import SideMenuContent from "./sidemenu/SideMenuContent";

class AppDrawer extends Component {
  render() {
    const { drawerOpen, toggleDrawer } = this.props;

    return (
      <>
        <Drawer
          className="Drawer"
          variant="persistent"
          anchor="left"
          open={drawerOpen}
          classes={{ paper: 'DrawerPaper' }}
        >
          <div className="DrawerControl">
            <IconButton onClick={toggleDrawer} edge="end">
              <ChevronLeft />
            </IconButton>
          </div>

          <Divider />

          <div className="DrawerContent">
            <DrawerHeader />

            <DrawerMenu />
          </div>
        </Drawer>
      </>
    );
  }
}

export default AppDrawer;
