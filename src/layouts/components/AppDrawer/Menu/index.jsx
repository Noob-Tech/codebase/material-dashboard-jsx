import React from 'react';
import { connect } from 'react-redux';
import { List } from '@material-ui/core';
import navigation from '../../../../configs/navigation.config';
import MenuItem from './Item';
// eslint-disable-next-line import/no-cycle
import MenuGroup from './Group';

export const MenuBuilder = connect(({ authState: { auth } }) => ({ auth }))(({ menu = [], auth }) => {
  const { role } = auth;

  return menu.map((item) => {
    const { permissions } = item;
    if (Array.isArray(permissions) && role !== undefined) {
      if (!permissions.includes(role)) return null;
    }

    switch (item.type) {
      case 'item':
        return <MenuItem key={item.id} item={item} />;
      case 'group':
        return <MenuGroup key={item.id} group={item} />;
      default:
        return null;
    }
  });
});

export default function DrawerMenu() {
  return (
    <List component="nav" className="DrawerMenu">
      <MenuBuilder menu={navigation} />
    </List>
  );
}
