import React, { useState } from 'react';
import classNames from 'classnames';
import { Collapse, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
// eslint-disable-next-line import/no-cycle
import { MenuBuilder } from './index';

export default function MenuGroup({ group }) {
  const { title, icon: Icon, omit, expanded } = group;

  const [open, setOpen] = useState(expanded === true);

  let icon;
  if (Icon !== undefined) {
    if (React.isValidElement(Icon)) {
      icon = Icon;
    } else if (Icon instanceof Object) {
      icon = <Icon />;
    }
  }

  let { navigations } = group;

  if (!Array.isArray(navigations)) {
    navigations = [];
  }

  return (
    <div className={classNames('DrawerMenu__Group', { 'display-hidden': omit === true })}>
      <ListItem className="DrawerMenu__Item" onClick={() => setOpen(!open)} button>
        {icon && <ListItemIcon className="Item__Icon">{icon}</ListItemIcon>}
        <ListItemText classes={{ primary: 'Item__Text' }} primary={title} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className="SubGroup">
          <MenuBuilder menu={navigations} />
        </List>
      </Collapse>
    </div>
  );
}
