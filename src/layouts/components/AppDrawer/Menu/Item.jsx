import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

export default function MenuItem({ item = {} }) {
  const { title, linkTo, icon: Icon, omit } = item;

  let icon;
  if (Icon !== undefined) {
    if (React.isValidElement(Icon)) {
      icon = Icon;
    } else if (Icon instanceof Object) {
      icon = <Icon />;
    }
  }

  return (
    <ListItem
      component={Link}
      to={linkTo || '/'}
      className={classNames('DrawerMenu__Item', { 'display-hidden': omit === true })}
      button
    >
      {icon && <ListItemIcon className="Item__Icon">{icon}</ListItemIcon>}
      <ListItemText classes={{ primary: 'Item__Text' }} primary={title} />
    </ListItem>
  );
}
