import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import Logo from 'assets/img/logo/logo.png';

class DrawerHeader extends Component {
  render() {
    return (
      <div className="DrawerHeader">
        <Link to="/">
          <img src={Logo} alt={process.env.APP_NAME} />
          <Typography variant="h6" className="DrawerHeader__Title">
            {process.env.APP_NAME}
          </Typography>
        </Link>
      </div>
    );
  }
}

export default DrawerHeader;
