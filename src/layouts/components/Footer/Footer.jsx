import React from 'react';
import { Divider } from '@material-ui/core';

const Footer = ({ className }) => (
  <div className={`Footer ${className}`}>
    <Divider />
    <div className="FooterContent">This is footer</div>
  </div>
);

export default Footer;
