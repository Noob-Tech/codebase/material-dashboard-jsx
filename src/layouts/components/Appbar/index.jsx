import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, useScrollTrigger, IconButton } from '@material-ui/core';
import { Menu } from '@material-ui/icons';

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const Appbar = ({ drawerOpen = false, toggleDrawer, ...props }) => (
  <>
    <ElevationScroll {...props}>
      <AppBar position="fixed" className={classNames('AppBar', { DrawerOpen: drawerOpen })}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="AppBar AppDrawer"
            onClick={toggleDrawer}
            edge="start"
            className="MenuIcon"
          >
            <Menu />
          </IconButton>
          THIS WILL BE TOOLBAR
        </Toolbar>
      </AppBar>
    </ElevationScroll>
    <Toolbar />
  </>
);

export default Appbar;
