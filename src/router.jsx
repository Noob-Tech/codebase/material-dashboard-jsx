import React, { lazy } from 'react';
import RouteContext from 'utilities/context/Route.context';
import RouterContext from 'utilities/context/Router.context';
import history from './history';

// Pages
const Dashboard = lazy(() => import(/* webpackChunkName: "dashboard" */ './views/dashboard'));
const SecuredView = lazy(() => import('./views/example-secured-views'));

function Router() {
  return (
    <>
      <RouterContext history={history}>
        <RouteContext exact path="/" component={Dashboard} />
        <RouteContext path="/personal" component={<div>This unatuhozied</div>} fullLayout />
        <RouteContext path="/secured-path" component={SecuredView} secured />
      </RouterContext>
    </>
  );
}

export default Router;
