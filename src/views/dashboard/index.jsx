import React from 'react';
import { connect } from 'react-redux';
import { Button, Typography } from '@material-ui/core';
import { LockOpen, Lock } from '@material-ui/icons';
import { authorize, signOut } from 'actions';

function Dashboard({ auth }) {
  const { authenticated } = auth;

  return (
    <div className="FlexContainer--Center--Col">
      <Typography variant="h4">THIS DASHBOARD</Typography>
      <Button
        style={{ marginTop: '1rem' }}
        variant="outlined"
        onClick={authenticated ? signOut : authorize}
        size="large"
        startIcon={authenticated ? <LockOpen /> : <Lock />}
      >
        <Typography variant="button">
          {authenticated ? 'Signing out ' : 'Authorize '}
          Me
        </Typography>
      </Button>
    </div>
  );
}

const mapStateToProps = ({ authState: { auth } }) => ({ auth });

export default connect(mapStateToProps)(Dashboard);
