import React from 'react';
import { Typography } from '@material-ui/core';

export default function ExampleSecuredView() {
  return (
    <div className="FlexContainer--Center--Col">
      <Typography variant="h6">Secured View</Typography>
      <Typography variant="caption">You are authorized to view this page</Typography>
    </div>
  );
}
