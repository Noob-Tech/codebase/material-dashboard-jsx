import React from 'react';
import { Container, Button, Link, Typography } from '@material-ui/core';

import HomeOutlined from '@material-ui/icons/HomeOutlined';

class NotAuthorized extends React.Component {
  render() {
    return (
      <Container className="FlexContainer">
        <div className="FlexContainer--Center--Col">
          <Typography variant="h1">403</Typography>
          <Typography variant="h2">Access Not Authorized</Typography>
          <Button
            style={{ marginTop: '1rem' }}
            href={process.env.PUBLIC_URL}
            variant="outlined"
            startIcon={<HomeOutlined />}
            component={Link}
            underline="none"
            size="large"
          >
            <Typography variant="button">Back to home</Typography>
          </Button>
        </div>
      </Container>
    );
  }
}
export default NotAuthorized;
