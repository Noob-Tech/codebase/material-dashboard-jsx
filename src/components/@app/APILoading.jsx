import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { CircularProgress } from '@material-ui/core';

function APILoading({ APICall }) {
  return (
    <div
      className={classNames('zindex-4 position-absolute w-100 h-100 justify-content-center align-items-center', {
        'd-none': !APICall,
        'd-flex': APICall,
      })}
    >
      <CircularProgress />
    </div>
  );
}

const mapStateToProps = ({ appState }) => ({
  APICall: appState.APICall,
});
export default connect(mapStateToProps)(APILoading);
