import React from 'react';
import { CircularProgress } from '@material-ui/core';

export default function FallbackLoading() {
  return (
    <div className="FallbackLoading">
      <CircularProgress />
    </div>
  );
}
