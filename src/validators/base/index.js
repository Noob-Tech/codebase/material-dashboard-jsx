import BaseValidator from './base.validator';

export default new BaseValidator();

export function validateObject(validator, o) {
  const errValidation = validator(o);
  if (errValidation !== true) {
    throwError('Validation Error', { metadata: errValidation });
  }
}
