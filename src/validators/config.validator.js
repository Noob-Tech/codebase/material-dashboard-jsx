import validator from './base';

export const validateClientConfig = validator.compile({
  baseURL: 'string|empty:false|required',
  basePath: 'string|empty:false|required',
});
