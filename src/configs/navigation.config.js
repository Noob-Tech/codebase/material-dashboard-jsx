import React from 'react';
import { Home, SupervisedUserCircle, AccountCircle, Group, VerifiedUser } from '@material-ui/icons';
import { USER_ROLE } from 'CONSTANTS';

const navigationConfig = [
  {
    id: 'home',
    title: 'Home',
    type: 'item',
    icon: <Home />,
    permissions: [USER_ROLE.PUBLIC],
    linkTo: '/',
  },
  {
    id: 'hidden-item',
    title: 'This item is omitted',
    type: 'item',
    omit: true,
  },
  {
    id: 'users',
    title: 'Users',
    type: 'group',
    icon: SupervisedUserCircle,
    expanded: true,
    navigations: [
      {
        id: 'personal',
        title: 'Personal',
        type: 'item',
        icon: <AccountCircle />,
        permissions: [USER_ROLE.PUBLIC],
        linkTo: '/personal',
      },
      {
        id: 'groups',
        title: 'Groups',
        type: 'group',
        icon: <Group />,
        navigations: [
          {
            id: 'group',
            title: 'Membership',
            type: 'item',
            icon: VerifiedUser,
            permissions: [USER_ROLE.PUBLIC],
            linkTo: '/membership',
          },
        ],
      },
    ],
  },
  {
    id: 'secured-path',
    title: 'Secured Path',
    linkTo: '/secured-path',
    type: 'item',
    icon: <VerifiedUser />,
    permissions: [USER_ROLE.PUBLIC],
  },
  {
    id: 'logout',
    title: 'Logout',
    type: 'item',
  },
];

export default navigationConfig;
