import { USER_ROLE } from 'CONSTANTS';

const routeConfig = {
  '/personal': {
    permissions: [USER_ROLE.PERSONAL],
  },
};

export default routeConfig;
