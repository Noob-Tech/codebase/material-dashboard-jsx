const { CLIENT_ID, CLIENT_SECRET, API_BASE_URL, API_BASE_PATH } = process.env;

const baseUrl = API_BASE_URL;
const basePath = API_BASE_PATH;
const clientConfig = {
  baseUrl,
  basePath,
};

const clientCredential = {
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
};

module.exports = {
  clientConfig,
  clientCredential,
};
