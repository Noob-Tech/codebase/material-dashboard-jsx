import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';

import { routerMiddleware } from 'connected-react-router';

import storage from 'redux-persist/lib/storage/session';
import thunk from 'redux-thunk';
import rootReducers from './reducers';
import history from '../history';

const enhancers = [];
const middlewares = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const { devToolsExtension } = window;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['authState'],
  blacklist: ['router'],
};

const persistedReducer = persistReducer(persistConfig, rootReducers(history));
const composedEnhancers = compose(applyMiddleware(...middlewares), ...enhancers);

const store = createStore(persistedReducer, {}, composedEnhancers);
const persistor = persistStore(store);

export default store;
export { persistor };
