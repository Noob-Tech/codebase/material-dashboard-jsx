import { push, goBack, replace } from 'connected-react-router';
import store from '../store';

const { dispatch } = store;

export const navigateTo = (page, options = { useReplace: false }) => {
  if (options.useReplace) {
    return dispatch(replace(page));
  }
  return dispatch(push(page));
};

export const navigateBack = () => dispatch(goBack());
