import { LOGIN, LOGOUT } from '../events';
import store from '../store';

const { dispatch } = store;

export const authorize = () => {
  dispatch({ type: LOGIN });
};

export const signOut = () => {
  dispatch({ type: LOGOUT });
};
