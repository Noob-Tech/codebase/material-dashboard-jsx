const initState = {
  APICall: false,
};

const appState = (state = initState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default appState;
