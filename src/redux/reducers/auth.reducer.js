import { USER_ROLE } from 'CONSTANTS';
import { LOGIN, LOGOUT } from '../events';

const initState = {
  auth: {
    authenticated: false,
    role: USER_ROLE.PUBLIC,
  },
  profile: {},
};

const authState = (prevState = initState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...prevState,
        auth: {
          ...prevState.auth,
          authenticated: true,
        },
      };
    case LOGOUT:
      return initState;
    default:
      return prevState;
  }
};

export default authState;
