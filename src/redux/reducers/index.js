import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import appState from './app.reducer';
import authState from './auth.reducer';

const reducers = (history) =>
  combineReducers({
    router: connectRouter(history),
    appState,
    authState,
  });

export default reducers;
