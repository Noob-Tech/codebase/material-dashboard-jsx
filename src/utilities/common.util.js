import { customAlphabet } from 'nanoid';

export function checkUndefined({ ...args }) {
  Object.keys(args).forEach((arg) => {
    if (args[arg] === undefined || args[arg] === '') throw new Error(`Missing this value: ${arg}`);
  });
}

export const filterNullValue = (o) => {
  const isArray = Array.isArray(o);

  if (isArray) {
    const out = [];
    o.forEach((val) => {
      if (val === undefined || val === null) return;
      out.push(val);
    });

    return out;
  }
  return Object.keys(o).reduce((accum, curr) => {
    const val = o[curr];
    if (val === undefined || val === null) return accum;

    accum[curr] = val;
    return accum;
  }, {});
};

export function generateId(n = 6) {
  return customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', n)();
}

export function objectValToKey(obj) {
  Object.keys(obj).forEach((key) => {
    obj[obj[key]] = key;
  });
  return obj;
}

export function validateEmail(email, allowEmpty = false) {
  if (!email && allowEmpty) return;
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isEmail = re.test(email);
  if (!isEmail) {
    throw new Error('Email not valid');
  }
}
