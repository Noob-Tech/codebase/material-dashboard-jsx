import React, { Suspense } from 'react';
import propTypes from 'prop-types';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';
import { FallbackLoading } from '@app';
import { VerticalLayout, FullPageLayout } from '../../layouts';
import routeConfig from '../../configs/route.config';

const RedirectToNotAuthorized = ({ path }) => (
  <Route path={path} exact>
    <Redirect to="/not-authorized" />
  </Route>
);

const RouteContext = connect(({ authState: { auth } }) => ({ auth }))(
  ({ path, component: Component, exact = false, fullLayout = false, secured = false, auth, ...rest }) => {
    const { role, authenticated } = auth;

    if (secured && !authenticated) {
      return <RedirectToNotAuthorized path={path} />;
    }

    // Get config
    const config = routeConfig[path];
    if (config) {
      const { permissions } = config;
      if (Array.isArray(permissions)) {
        if (!permissions.includes(role)) {
          return <RedirectToNotAuthorized path={path} />;
        }
      }
    }

    return (
      <Route
        path={path}
        exact={exact}
        {...rest}
        render={(props) => (
          <Suspense fallback={<FallbackLoading />}>
            {fullLayout ? (
              <FullPageLayout>
                <Component {...props} />
              </FullPageLayout>
            ) : (
              <VerticalLayout>
                <Component {...props} />
              </VerticalLayout>
            )}
          </Suspense>
        )}
      />
    );
  }
);

RouteContext.propTypes = {
  path: propTypes.string.isRequired,
  component: propTypes.oneOfType([propTypes.element, propTypes.object]).isRequired,
  exact: propTypes.bool,
  fullLayout: propTypes.bool,
  secured: propTypes.bool,
};

export default RouteContext;
