import React, { lazy } from 'react';
import { Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import propTypes from 'prop-types';
import RouteContext from './Route.context';

const NotFoundError = lazy(() => import(/* webpackChunkName: `404` */ '../../views/misc/error/404'));
const InternalError = lazy(() => import(/* webpackChunkName: `500` */ '../../views/misc/error/500'));
const NotAuthorized = lazy(() => import(/* webpackChunkName: `403` */ '../../views/misc/NotAuthorized'));

const RouterContext = ({ history, children }) => (
  <ConnectedRouter history={history}>
    <Switch>
      {children}

      <RouteContext path="/not-authorized" component={NotAuthorized} fullLayout />
      <RouteContext path="/internal-error" component={InternalError} fullLayout />
      <RouteContext path="*" component={NotFoundError} fullLayout />
    </Switch>
  </ConnectedRouter>
);

RouterContext.propTypes = {
  history: propTypes.object.isRequired,
  children: propTypes.oneOfType([propTypes.arrayOf(propTypes.element), propTypes.element]),
};

export default RouterContext;
