export default class DateUtil {
  static fromEpochSecond(sec) {
    const d = new Date(0);
    d.setUTCSeconds(sec);
    return d;
  }

  static nowToEpochSecond() {
    return Math.floor(new Date().getTime() / 1000);
  }

  static nowToEpochMilliSecond() {
    return new Date().getTime();
  }
}
