module.exports = {
  plugins: [
    ['@babel/plugin-syntax-dynamic-import'],
    [
      'babel-plugin-transform-imports',
      {
        '@material-ui/core': {
          // Use "transform: '@material-ui/core/${member}'," if your bundler does not support ES modules
          transform: (member) => `@material-ui/core/esm/${member}`,
          preventFullImport: true,
        },
        '@material-ui/icons': {
          // Use "transform: '@material-ui/icons/${member}'," if your bundler does not support ES modules
          transform: (member) => `@material-ui/icons/esm/${member}`,
          preventFullImport: true,
        },
      },
    ],
    [
      '@babel/plugin-proposal-class-properties',
      {
        loose: true,
      },
    ],
  ],
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: '12',
        },
      },
    ],
    '@babel/preset-react',
  ],
};
