# Material UI Dashboard Template

## Prerequisites

1. Node.js v12

## Set-up

### Development Config

1. Copy [config.development.example.json](config.development.example.json) as [config.development.json](config.development.json)

```bash
cp ./config.development.example.json ./config.development.json
```

2. Belows are available configurations:

| Key | Description | Required | Values |
| ---- | ------- | ----- | ----------- |
| `CLIENT_ID` | API Client Id | **✓** | String |
| `CLIENT_SECRET` | API Client Secret | **✓** | Number |
| `USE_MOCK` | Set `true` to use @fake-db | **✓** | String |
| `BASE_URL` | API Base URL | **✓** | String |
| `BASE_PATH` | API Base Path | **✓** | String |
| `PUBLIC_URL` | Asset PUBLIC_URL | | String |
| `APP_NAME` | Application Name | **✓** | String |


### Install Dependencies

```bash
npm install
```

## Development

### Start App Development Runtime

```bash
npm run start:dev
```

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>
