/* eslint-disable */

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const ESLintWebpackPlugin = require('eslint-webpack-plugin');

module.exports = () => {
  const { NODE_ENV } = process.env;
  const isDev = NODE_ENV !== 'production';

  let plugins = [];
  let config;
  if (isDev) {
    config = require(path.resolve(__dirname, './config.development.json'));
    plugins.push(
      new ESLintWebpackPlugin({
        extensions: ['js', 'jsx'],
      })
    );
  } else {
    config = require(path.resolve(__dirname, './config.production.json'));
  }

  // Define public url
  config.PUBLIC_URL = config.PUBLIC_URL || '/';

  return {
    devtool: 'eval-cheap-module-source-map',
    mode: isDev ? 'development' : 'production',
    entry: {
      bundle: path.resolve(__dirname, './src/index.js'),
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: ['babel-loader'],
        },
        {
          test: /\.(css|scss)$/,
          exclude: /node_modules/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                importLoaders: 1,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  sourceMap: isDev,
                  plugins: ['postcss-preset-env', 'postcss-import', 'postcss-nested', 'autoprefixer'],
                },
              },
            },
            'resolve-url-loader',
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
          use: ['url-loader?limit=10000&name=img/[name].[hash:8].[ext]'],
        },
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: '[name].[contenthash:8].js',
      sourceMapFilename: '[name].[fullhash:8].map',
      chunkFilename: '[id].[fullhash:8].js',
    },
    plugins: [
      ...plugins,
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        template: `${__dirname}/public/index.html`,
        filename: 'index.html',
        inject: 'body',
        favicon: './public/favicon.ico',
      }),
      new OptimizeCSSAssetsPlugin({}),
      new WebpackManifestPlugin(),
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
        process: ['process'],
      }),
      new webpack.DefinePlugin({
        'process.env': JSON.stringify(config),
      }),
    ],
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            // cacheGroupKey here is `commons` as the key of the cacheGroup
            name(module, chunks, cacheGroupKey) {
              const moduleFileName = module
                .identifier()
                .split('/')
                .reduceRight((item) => item);
              return `${cacheGroupKey}-${moduleFileName}`;
            },
            chunks: 'all',
          },
        },
      },
    },
    devServer: {
      contentBase: path.resolve(__dirname, './src'),
      hot: true,
      inline: true,
      port: 3000,
      open: true,
    },
    resolve: {
      extensions: ['.js', '.jsx', '.png', '.jpg', '.jpeg', 'svg'],
      alias: {
        '@app': path.resolve(__dirname, './src/components/@app'),
        utilities: path.resolve(__dirname, './src/utilities'),
        actions: path.resolve(__dirname, './src/redux/actions'),
        assets: path.resolve(__dirname, './src/assets'),
        validators: path.resolve(__dirname, './src/validators'),
        configs: path.resolve(__dirname, './src/configs'),
        CONSTANTS: path.resolve(__dirname, './src/constants'),
      },
    },
  };
};
