module.exports = {
  extends: ["stylelint-config-standard"],
  plugins: ["stylelint-scss"],
  rules: {
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: ["extend", "extends", "tailwind"],
      },
    ],
  },
  ignoreFiles: ["node_modules/**/*.css"],
};
